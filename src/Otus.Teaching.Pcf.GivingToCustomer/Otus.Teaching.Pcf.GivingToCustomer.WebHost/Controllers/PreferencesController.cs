﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;


namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;
        const string nowKey = "preferences";
        public PreferencesController(IRepository<Preference> preferencesRepository, IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            string serialized = await _distributedCache.GetStringAsync(nowKey);
            if (serialized != null)
            {
                return Ok(JsonSerializer.Deserialize<List<PreferenceResponse>>(serialized));
            }
            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(response),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(10)
                });

            return Ok(response);
        }
    }
}